$(document).ready(function() {
    $("#taskform").submit(function(e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: 'index.php',
            data: $(this).serialize(),
            success: function(response) {
                // console.log(response)
                // $('#task').html(response);
                var jsonData = JSON.parse(response)
                console.log(jsonData);
                alert("Task has been succesfully inserted!");
                $.get('tasks.txt', function(data) {
                    var task;
                    $.each(data.split(/[\n\r]+/), function(index, value) {
                        task += '<li>' + value + '</li>'
                    });
                    $("#task").html(task);
                });
            }
        })
    })
})